﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{
	enum State
	{
		COUNTDOWN = 0,
		PLAY = 1,
		END_SCREEN = 2
	};

	[SerializeField]
	private Transform player;
	[SerializeField]
	private UIController uiController;
	[SerializeField]
	private GameObject startPattern;
	[SerializeField]
	private GameObject[] patterns;

	private float patternSize = 200f;

	private List<GameObject> patternsLive;
	private int indexPattern = 3;

	private float countdownTimer = 3.99f;
	private float timer = 0f;

	private State currentState = State.COUNTDOWN;

	private void Awake()
	{
		timer = 0f;
		countdownTimer = 3.99f;
	
		player.GetComponent<PlayerController>().SpeedMultiplicator = 0f;

		patternsLive = new List<GameObject>();

		patternsLive.Add(Instantiate(startPattern, new Vector3(0f, transform.position.y, 0f), Quaternion.identity, transform));
		patternsLive.Add(Instantiate(patterns[Random.Range(0, patterns.Length)], new Vector3(0f, transform.position.y, patternSize), Quaternion.identity, transform));
		patternsLive.Add(Instantiate(patterns[Random.Range(0, patterns.Length)], new Vector3(0f, transform.position.y, patternSize * 2), Quaternion.identity, transform));
		indexPattern = 3;

		uiController.UpdateTimer(timer);
		StartCoutdown();
	}

	public void PatternDone(GameObject patternDone)
	{
		if (patternsLive.IndexOf(patternDone) > 0)
		{
			patternsLive.Add(Instantiate(patterns[Random.Range(0, patterns.Length)], new Vector3(0f, transform.position.y, patternSize * indexPattern), Quaternion.identity, transform));
			++indexPattern;
		}

		if (patternsLive.Count >= 4)
		{
			Destroy(patternsLive[0]);
			patternsLive.RemoveAt(0);
		}
	}

	public void Reset()
	{
		foreach (GameObject pattern in patternsLive)
			Destroy(pattern);

		player.GetComponent<PlayerController>().PlayerReset();
		uiController.ShowHUD(true);

		Awake();
	}

	public void StartCoutdown()
	{
		countdownTimer = 3.99f;
		currentState = State.COUNTDOWN;
	}

	private void LateUpdate()
	{
		switch(currentState)
		{
			case State.COUNTDOWN:
				countdownTimer -= Time.deltaTime;

				if (countdownTimer < 1f)
				{
					uiController.UpdateCountdown("");
					player.GetComponent<PlayerController>().SpeedMultiplicator = 1f;
					currentState = State.PLAY;
				}
				else
				{
					uiController.UpdateCountdown(((int)countdownTimer).ToString());
				}
				break;
			case State.PLAY:
				timer += Time.deltaTime;
				uiController.UpdateTimer(timer);
				break;
			case State.END_SCREEN:
				if (Input.GetKeyDown(KeyCode.Return))
					Reset();
				break;
		}
	}

	public void GameOver()
	{
		GetComponent<PivotManager>().StopMoving();
		player.GetComponent<PlayerController>().Dead();
		uiController.InitEndScreen(timer);
		currentState = State.END_SCREEN;
	}
}