﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
	[SerializeField]
	private HUDController HUD;
	[SerializeField]
	private EndScreenController endScreen;

	/* HUD */
	public void ShowHUD(bool Show)
	{
		ShowEndScreen(false);
		HUD.gameObject.SetActive(Show);
	}

	public void UpdateCountdown(string countDown)
    {
		HUD.UpdateCountdown(countDown);
	}

	public void UpdateTimer(float Time)
	{
		HUD.UpdateTimer(Time);
	}

	public void InitEndScreen(float Timer)
	{
		ShowHUD(false);
		ShowEndScreen(true);
		endScreen.InitTime(Timer);
	}

	/* END SCREEN */
	public void ShowEndScreen(bool Show)
	{
		endScreen.gameObject.SetActive(Show);
	}
}
