﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndScreenController : MonoBehaviour
{
	[SerializeField]
	private Text time;

	public void InitTime(float Time)
	{
		int minutes = (int)(Time / 60f);
		int seconds = (int)(Time - (minutes * 60f));
		int miliseconds = (int)((Time - seconds - (minutes * 60f)) * 1000);

		time.text = string.Format("Time:\n{0}", string.Format("{0:D2}:{1:D2}:{2:D3}", minutes, seconds, miliseconds));
	}
}
