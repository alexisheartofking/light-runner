﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlashingText : MonoBehaviour
{
	public float Speed = 2f;

	private Text textToFlash;

	private void Awake()
	{
		textToFlash = GetComponent<Text>();
	}

	void Update()
    {
		textToFlash.material.color = new Color(textToFlash.material.color.r, textToFlash.material.color.g, textToFlash.material.color.b, (Mathf.Sin(Time.time * Speed) + 1f) / 2f);
	}
}
