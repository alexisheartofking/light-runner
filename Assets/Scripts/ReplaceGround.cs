﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ReplaceGround : MonoBehaviour
{
	[SerializeField]
	private int place;
	[SerializeField]
	private float distanceToMid = 5f;

	private Transform pivot;

    void Start()
    {
		pivot = this.transform.parent;
    }

    void Update()
    {
		Vector3 finalPosition = Quaternion.AngleAxis(PivotManager.RotationValue * place, -Vector3.back) * new Vector3(0f, -distanceToMid, 0f);
		this.transform.localPosition = new Vector3(finalPosition.x, finalPosition.y, this.transform.localPosition.z);
		this.transform.localEulerAngles = new Vector3(0f, 0f, PivotManager.RotationValue * place);
	}
}
