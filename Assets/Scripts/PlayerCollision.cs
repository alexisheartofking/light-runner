﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
	[SerializeField]
	private LevelController levelController;

	private void OnTriggerEnter(Collider other)
	{
		if (other.tag == "DeadZone")
		{
			levelController.GameOver();
		}
		else if (other.tag == "EndZone")
		{
			levelController.PatternDone(other.transform.parent.gameObject);
		}
		else if (other.tag == "Block")
		{
			levelController.GameOver();
		}
	}

	public bool IsGrounded()
	{
		RaycastHit raycastResult;

		if (Physics.Raycast(transform.position + (transform.forward / 2f), Vector3.down, out raycastResult, 1f) && raycastResult.transform.tag == "Ground"
			|| Physics.Raycast(transform.position - (transform.forward / 2f), Vector3.down, out raycastResult, 1f) && raycastResult.transform.tag == "Ground")
		{
			GetComponent<PlayerController>().SpeedMultiplicator = raycastResult.transform.GetComponent<GroundSpeedMultiplicator>().SpeedMultiplicator;
			return true;
		}

		return false;
	}
}
