﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadZone : MonoBehaviour
{
	[SerializeField]
	private Transform player;

	void Update()
	{
		this.transform.localPosition = new Vector3(player.localPosition.x, this.transform.localPosition.y, player.localPosition.z);
	}
}
