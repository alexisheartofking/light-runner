﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PivotManager : MonoBehaviour
{
	public static float RotationValue = 45f;

	public AnimationCurve MoveAnimationCurve;
	public float MoveAnimationTime;

	private float moveAnimationTime;

	private float moveAnimationTimer = 0f;
	private bool isMoving = false;

	private float baseRotation = 0f;
	private float targetRotation = 0f;

	public void Reset()
	{
		this.transform.eulerAngles = Vector3.zero;
		isMoving = false;
	}

	public bool Move(bool Left, float SpeedMultiplicator = 1f)
	{
		if (isMoving)
			return false;

		if (Left)
		{
			targetRotation = this.transform.eulerAngles.z + RotationValue;
		}
		else
		{
			targetRotation = this.transform.eulerAngles.z - RotationValue;
		}

		moveAnimationTime = MoveAnimationTime / SpeedMultiplicator;
		baseRotation = this.transform.eulerAngles.z;
		moveAnimationTimer = 0f;
		isMoving = true;

		return true;
	}

	public void StopMoving()
	{
		isMoving = false;
	}

	private void Update()
	{
		if (isMoving)
		{
			moveAnimationTimer += Time.deltaTime;

			if (moveAnimationTimer >= moveAnimationTime)
			{
				moveAnimationTimer = moveAnimationTime;

				this.transform.localEulerAngles = new Vector3(0f, 0f, targetRotation);

				isMoving = false;
			}
			else
			{
				this.transform.localEulerAngles = new Vector3(0f, 0f, baseRotation + ((targetRotation - baseRotation) * MoveAnimationCurve.Evaluate(moveAnimationTimer / moveAnimationTime)));
			}
		}
	}
}
