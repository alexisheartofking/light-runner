﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDController : MonoBehaviour
{
	[SerializeField]
	private Text countdownText;
	[SerializeField]
	private Text TimerText;

	public void UpdateCountdown(string countDown)
	{
		countdownText.text = countDown;
	}

	public void UpdateTimer(float Time)
	{
		int minutes = (int)(Time / 60f);
		int seconds = (int)(Time - (minutes * 60f));
		int miliseconds = (int)((Time - seconds - (minutes * 60f)) * 1000);

		TimerText.text = string.Format("{0:D2}:{1:D2}:{2:D3}", minutes, seconds, miliseconds);
	}
}
