﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	[SerializeField]
	private GameObject DeathFXPrefab;

	private GameObject currentDeathFX;

	public AnimationCurve YAxisAnimationCurve;
	public AnimationCurve ZRotationAnimationcurve;

	public float SpeedMultiplicator = 1f;

	[SerializeField]
	private PivotManager pivotManager;

	private float speed = 20f;
	private bool isSideMoving = false;
	private bool isGrounded = true;

	private float MoveAnimationTime = 0f;
	private float MoveAnimationTimer = 0f;

	private float targetRotation = 0f;

	void Start()
	{
		MoveAnimationTime = pivotManager.MoveAnimationTime;
	}

	public void PlayerReset()
	{
		this.transform.localPosition = Vector3.zero;
		this.transform.eulerAngles = Vector3.zero;

		pivotManager.Reset();
		isGrounded = true;
		isSideMoving = false;

		if (currentDeathFX != null)
			Destroy(currentDeathFX);

		gameObject.SetActive(true);
	}

	public void Dead()
	{
		SpeedMultiplicator = 0f;
		gameObject.SetActive(false);

		currentDeathFX = Instantiate(DeathFXPrefab, transform.position, Quaternion.identity);
	}

	void Update()
	{
		if (Input.GetKey(KeyCode.Escape))
		{
			Application.Quit();
		}

		if (SpeedMultiplicator <= 0f)
			return;

		this.transform.localPosition += new Vector3(0f, 0f, (speed * SpeedMultiplicator) * Time.deltaTime);
		isGrounded = GetComponent<PlayerCollision>().IsGrounded();

		if (isSideMoving == false)
		{
			if (isGrounded == true)
			{
				if (Input.GetKey(KeyCode.Q))
				{
					if (pivotManager.Move(true))
					{
						isSideMoving = true;
						targetRotation = 90f;
						MoveAnimationTimer = 0f;
					}
				}
				else if (Input.GetKey(KeyCode.D))
				{
					if (pivotManager.Move(false))
					{
						isSideMoving = true;
						targetRotation = -90f;
						MoveAnimationTimer = 0f;
					}
				}

				this.transform.localPosition = new Vector3(this.transform.localPosition.x, 0f, this.transform.localPosition.z);
			}
			else
			{
				this.transform.localPosition += new Vector3(0f, -(speed * Time.deltaTime), 0f);
			}
		}
		else
		{
			MoveAnimationTimer += Time.deltaTime;

			if (MoveAnimationTimer >= MoveAnimationTime)
			{
				this.transform.localPosition = new Vector3(this.transform.localPosition.x, 0f, this.transform.localPosition.z);
				this.transform.eulerAngles = new Vector3(0f, 0f, 0f);
				isSideMoving = false;
			}
			else
			{
				float percent = MoveAnimationTimer / MoveAnimationTime;

				this.transform.localPosition = new Vector3(this.transform.localPosition.x, YAxisAnimationCurve.Evaluate(percent), this.transform.localPosition.z);
				this.transform.eulerAngles = new Vector3(0f, 0f, targetRotation * ZRotationAnimationcurve.Evaluate(percent));
			}
		}
	}
}
