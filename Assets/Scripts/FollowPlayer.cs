﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
	[SerializeField]
	private Transform player;
	[SerializeField]
	Vector3 armLength = Vector3.zero;

	void LateUpdate()
    {
		this.transform.position = player.position + armLength;
	}
}
